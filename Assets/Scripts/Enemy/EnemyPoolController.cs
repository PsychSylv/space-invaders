using SpaceInvader.Item.Bullet;
using SpaceInvader.Item.Enemy.Controller;
using SpaceInvader.Item.Type;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvader.Item.Enemy.Pool
{
    public class EnemyPoolController : MonoBehaviour
    {
        [SerializeField] private EnemyController[] enemyControllers;
        [SerializeField] private EnemyController[] enemy;
        [SerializeField] private BulletController[] bulletControllers;
        [SerializeField] private BulletController bullet;
        [SerializeField] private GameObject enemySpawnHolder;
        [SerializeField] private GameObject enemyBulletSpawnHolder;
        [SerializeField] private Vector3 initialPosition;

        [SerializeField] private float speedMultiplier;
        [SerializeField] private float movespeed;
        [SerializeField] private float minX;
        [SerializeField] private float maxX;
        [SerializeField] private float offsetX;
        [SerializeField] private float offsetY;
        [SerializeField] private int row;
        [SerializeField] private int column;

        [SerializeField] private int bulletToPool;

        private List<EnemyController> activeEnemy;
        private float moveX;
        private Vector3 currentSpawnPosition;
        private int random;
        private int counter;
        private int enemyType;
        private bool isMax = false;
        private bool canMove;

        private const int FirstCheckpoint = 1;
        private const int SecondCheckpoint = 3;

        public event Action<int> EnemyDestroyed;
        public event Action<ItemType> ItemHit;

        public bool CanMove 
        {
            get { return canMove; }
            set 
            { 
                canMove = value; 
                for (int i = 0; i < bulletControllers.Length; i++) 
                { 
                    bulletControllers[i].IsActive = value; 
                } 
            } 
        }

        private void Start()
        {
            counter = 0;
            currentSpawnPosition = initialPosition;

            activeEnemy = new List<EnemyController>();
            enemyControllers = new EnemyController[row * column];
            bulletControllers = new BulletController[bulletToPool];

            //Instantiate Enemies
            for(int i = 0; i < row; i++)
            {
                for(int j = 0; j < column; j++)
                {
                    enemyControllers[counter] = Instantiate(enemy[enemyType], Vector2.zero, Quaternion.identity, enemySpawnHolder.transform);
                    enemyControllers[counter].transform.position = new Vector2(currentSpawnPosition.x += offsetX, currentSpawnPosition.y);

                    enemyControllers[counter].HitEnemy += OnEnemyDestroyed;
                    counter++;
                }

                if(i == FirstCheckpoint || i == SecondCheckpoint)
                {
                    enemyType++;
                }

                currentSpawnPosition.y += offsetY;
                currentSpawnPosition.x = initialPosition.x;
            }

            InitializeActiveEnemy();

            //Instatiate Enemy Bullets
            for(int i = 0; i < bulletToPool; i++)
            {
                bulletControllers[i] = Instantiate(bullet, Vector2.zero, Quaternion.identity, enemyBulletSpawnHolder.transform);
                bulletControllers[i].Type = ItemType.Enemy;
                bulletControllers[i].gameObject.SetActive(false);
            }

            moveX = movespeed * Time.deltaTime;
        }

        private void OnDestroy()
        {
            for(int i = 0; i < enemyControllers.Length; i++)
            {
                enemyControllers[i].HitEnemy -= OnEnemyDestroyed;
            }
        }

        private void Update()
        {
            if (CanMove)
            {
                Move(); 
                GetPooledBullet();
            }
        }

        public int GetEnemyCount()
        {
            return enemyControllers.Length;
        }

        public void InitializePosition()
        {
            counter = 0;
            currentSpawnPosition = initialPosition;

            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    enemyControllers[counter].gameObject.SetActive(true);
                    enemyControllers[counter].transform.position = new Vector2(currentSpawnPosition.x += offsetX, currentSpawnPosition.y);
                    counter++;
                }

                currentSpawnPosition.y += offsetY;
                currentSpawnPosition.x = initialPosition.x;
            }

            InitializeActiveEnemy();

            moveX = movespeed * Time.deltaTime;
        }

        public void ResetPool()
        {
            for (int i = 0; i < enemyControllers.Length; i++)
            {
                enemyControllers[i].gameObject.SetActive(false);
                enemyControllers[i].ResetAnimatorSpeed();
            }

            for (int i = 0; i < activeEnemy.Count; i++)
            {
                activeEnemy.Remove(activeEnemy[i]);
            }

            for (int i = 0; i < bulletControllers.Length; i++)
            {
                bulletControllers[i].gameObject.SetActive(false);
            }
        }

        public void UpdateSpeed()
        {
            moveX *= speedMultiplier;

            for(int i = 0; i < enemyControllers.Length; i++)
            {
                enemyControllers[i].UpdateAnimatorSpeed();
            }
        }

        private void OnEnemyDestroyed(int score)
        {
            EnemyDestroyed?.Invoke(score);
            ItemHit?.Invoke(ItemType.Enemy);
        }

        private void Move()
        {
            for (int i = 0; i < enemyControllers.Length; i++)
            {
                enemyControllers[i].gameObject.transform.Translate(moveX, 0, 0);

                if(!isMax)
                {
                    if(enemyControllers[i].gameObject.activeInHierarchy)
                    {
                        if (enemyControllers[i].gameObject.transform.position.x >= maxX || enemyControllers[i].gameObject.transform.position.x <= minX)
                        {
                            isMax = true;
                        }
                    }
                }
            }

            if(isMax)
            {
                MoveDown();
            }
        }

        private void MoveDown()
        {
            for(int i = 0; i < enemyControllers.Length; i++)
            {
                enemyControllers[i].gameObject.transform.Translate(0, -offsetY, 0);
            }

            moveX *= -1;
            isMax = false;
        }

        private void GetPooledBullet()
        {
            for(int i = 0; i < bulletControllers.Length; i++)
            {
                if(!bulletControllers[i].gameObject.activeInHierarchy)
                {
                    BulletPosition(bulletControllers[i]);
                    bulletControllers[i].SpawnBullet();
                    bulletControllers[i].IsActive = true;
                }
            }
        }

        private void BulletPosition(BulletController bullet)
        {
            CheckActiveEnemy();

            random = UnityEngine.Random.Range(0, activeEnemy.Count);

            if (activeEnemy[random].gameObject.activeInHierarchy)
            {
                bullet.transform.position = activeEnemy[random].transform.position;
            }
            else
            {
                activeEnemy.Remove(activeEnemy[random]);
                BulletPosition(bullet);
            }
        }

        private void InitializeActiveEnemy()
        {
            for(int i = 0; i < enemyControllers.Length; i++)
            {
                activeEnemy.Add(enemyControllers[i]);
            }
        }

        private void CheckActiveEnemy()
        {
            for(int i = 0; i < enemyControllers.Length; i++)
            {
                if(!enemyControllers[i].gameObject.activeInHierarchy)
                {
                    activeEnemy.Remove(enemyControllers[i]);
                }
            }
        }
    }
}
