using SpaceInvader.Item.Type;
using System;
using UnityEngine;

namespace SpaceInvader.Item.Enemy.Controller
{
    public class EnemyController : MonoBehaviour, IDamageable
    {
        [SerializeField] private int score;
        [SerializeField] private float animatorSpeedMultiplier;
        [SerializeField] private Animator animator;

        public event Action<int> HitEnemy;
        public event Action<ItemType> HitItem;

        public ItemType Type { get; set; }

        private void Start()
        {
            Type = ItemType.Enemy;
        }

        public void ResetAnimatorSpeed()
        {
            animator.speed = 1f;
        }

        public void UpdateAnimatorSpeed()
        {
            animator.speed *= animatorSpeedMultiplier;
        }

        public void Damaged(ItemType type)
        {
            if (type == Type) return;
            else if (type == ItemType.None) return;

            HitEnemy?.Invoke(score);
            HitItem?.Invoke(Type);
            gameObject.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            try
            {
                IInteractable interactable = collision.GetComponent<IInteractable>();
                Damaged(interactable.Type);
            }
            catch
            {

            }
        }
    }
}
