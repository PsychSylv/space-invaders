using SpaceInvader.Item.Type;

namespace SpaceInvader.Item
{
    public interface IInteractable
    {
        public ItemType Type { get; set; }

        void Interact(ItemType type);
    }
}
