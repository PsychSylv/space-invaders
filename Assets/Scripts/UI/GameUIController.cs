using SpaceInvader.Game.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceInvader.UI.Controller
{
    public class GameUIController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text lifeText;

        private void Start()
        {
            gameManager.ScoreChanged += OnScoreChanged;
            gameManager.LifeChanged += OnLifeChanged;
        }

        private void OnDestroy()
        {
            gameManager.ScoreChanged -= OnScoreChanged;
            gameManager.LifeChanged -= OnLifeChanged;
        }

        private void OnScoreChanged(int score)
        {
            scoreText.text = "Score: " + score.ToString();
        }

        private void OnLifeChanged(int life)
        {
            lifeText.text = "Life: " + life.ToString();
        }
    }
}
