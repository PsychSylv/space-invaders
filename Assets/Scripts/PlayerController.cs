using SpaceInvader.Item;
using SpaceInvader.Item.Bullet;
using SpaceInvader.Item.Type;
using System;
using UnityEngine;

namespace SpaceInvader.Player.Controller
{
    public class PlayerController : MonoBehaviour, IDamageable
    {
        [SerializeField] private float movespeed;
        [SerializeField] private BulletController bullet;
        [SerializeField] private GameObject bulletHolder;
        [SerializeField] private Vector3 initialPosition;
        [SerializeField] private float minX;
        [SerializeField] private float maxX;

        private bool isActive;
        private BulletController bulletController;

        public event Action<ItemType> HitItem;
        public event Action ShootBullet;

        public ItemType Type { get; set; }
        public bool CanMove 
        {
            get { return isActive; }
            set { isActive = value;  bulletController.IsActive = value; } 
        }

        private void Start()
        {
            Type = ItemType.Player;

            bulletController = Instantiate(bullet, transform.position, Quaternion.identity, bulletHolder.transform);
        }
        private void Update()
        {
            if(CanMove)
            {
                Move();
                Shoot();
            }
        }

        public void Reset()
        {
            gameObject.transform.position = initialPosition;
            bulletController.gameObject.SetActive(false);
        }

        public void Damaged(ItemType type)
        {
            if (type == ItemType.Player) return;
            HitItem?.Invoke(Type);
        }

        private void Move()
        {
            if(Input.GetKey(KeyCode.A) && transform.position.x > minX)
            {
                gameObject.transform.Translate(Vector3.left * movespeed * Time.deltaTime);
            }
            else if(Input.GetKey(KeyCode.D) && transform.position.x < maxX)
            {
                gameObject.transform.Translate(Vector3.right * movespeed * Time.deltaTime);
            }
        }

        private void Shoot()
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                if(!bulletController.gameObject.activeInHierarchy)
                {
                    bulletController.gameObject.transform.position = transform.position;
                    bulletController.SpawnBullet();
                    bulletController.IsActive = true;
                    ShootBullet?.Invoke();
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            IInteractable interactable = collision.GetComponent<IInteractable>();
            Damaged(interactable.Type);
        }
    }
}
