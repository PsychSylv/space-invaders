using SpaceInvader.Item.Type;
using System;

namespace SpaceInvader.Item
{
    public interface IDamageable
    {
        ItemType Type { get; set; }
        public event Action<ItemType> HitItem;
        
        void Damaged(ItemType type);
    }
}
