namespace SpaceInvader.Item.Type
{
    public enum ItemType
    {
        None,
        Player,
        Enemy,
        Goal
    }
}
