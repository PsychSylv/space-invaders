namespace SpaceInvader.Game.State
{
    public enum GameState
    {
        Home,
        Playing,
        Paused,
        GameOver
    }
}
