using SpaceInvader.Game.Manager;
using SpaceInvader.Item.Type;
using UnityEngine;

namespace SpaceInvader.Audio.Controller
{
    public class GameAudioController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip playerShootClip;
        [SerializeField] private AudioClip playerDamagedClip;
        [SerializeField] private AudioClip enemyDamagedClip;

        private void Start()
        {
            gameManager.PlayerShoot += OnPlayerShoot;
            gameManager.ItemDamaged += OnItemDamaged;
        }

        private void OnDestroy()
        {
            gameManager.PlayerShoot -= OnPlayerShoot;
            gameManager.ItemDamaged -= OnItemDamaged;
        }

        private void OnPlayerShoot()
        {
            audioSource.PlayOneShot(playerShootClip);
        }

        private void OnItemDamaged(ItemType item)
        {
            if(item == ItemType.Player)
            {
                audioSource.PlayOneShot(playerDamagedClip);
            }
            else
            {
                audioSource.PlayOneShot(enemyDamagedClip);
            }
        }
    }
}
