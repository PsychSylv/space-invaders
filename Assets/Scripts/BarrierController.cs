using SpaceInvader.Item.Type;
using System;
using UnityEngine;

namespace SpaceInvader.Item.Barrier
{
    public class BarrierController : MonoBehaviour, IInteractable
    {
        public ItemType Type { get; set; }

        public event Action EnemyWin; 

        private void Start()
        {
            Type = ItemType.Goal;
        }

        public void Interact(ItemType type)
        {
            if(type == ItemType.Enemy)
            {
                EnemyWin?.Invoke();
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            try
            {
                IDamageable damageable = collision.GetComponent<IDamageable>();
                Interact(damageable.Type);
            }
            catch
            {
                return;
            }
        }
    }
}
