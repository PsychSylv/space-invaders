using SpaceInvader.Item.Type;
using UnityEngine;

namespace SpaceInvader.Item.Shield
{
    public class ShieldController : MonoBehaviour, IInteractable
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Sprite fullDurabilitySprite;
        [SerializeField] private Sprite halfDurabilitySprite;
        [SerializeField] private Sprite lowDurabilitySprite;
        [SerializeField] private Sprite veryLowDurabilitySprite;

        [SerializeField] private int life;

        private const int MaxLife = 4;

        public ItemType Type { get; set; }

        private void Start()
        {
            life = MaxLife;
            CheckDurability();
            Type = ItemType.None;
        }

        public void Reset()
        {
            life = MaxLife;
            gameObject.SetActive(true);
            CheckDurability();
        }

        public void Interact(ItemType type)
        {
            life--;
            CheckDurability();
        }

        private void CheckDurability()
        {
            switch(life)
            {
                case 4:
                    spriteRenderer.sprite = fullDurabilitySprite;
                    break;
                case 3:
                    spriteRenderer.sprite = halfDurabilitySprite;
                    break;
                case 2:
                    spriteRenderer.sprite = lowDurabilitySprite;
                    break;
                case 1:
                    spriteRenderer.sprite = veryLowDurabilitySprite;
                    break;
                case 0:
                    gameObject.SetActive(false);
                    break;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            try
            {
                IInteractable interactable = collision.GetComponent<IInteractable>();
                Interact(interactable.Type);
            }
            catch
            {
                IDamageable damageable = collision.GetComponent<IDamageable>();
                Interact(damageable.Type);
            }
        }
    }
}
