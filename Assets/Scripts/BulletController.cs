using SpaceInvader.Item.Type;
using UnityEngine;

namespace SpaceInvader.Item.Bullet
{
    public class BulletController : MonoBehaviour, IInteractable
    {
        [SerializeField] private ItemType itemType;
        [SerializeField] private float movespeed;

        private bool isActive = false;

        public bool IsActive { get { return isActive; } set { isActive = value; } }
        public ItemType Type { get; set; }

        private void Awake()
        {
            Type = itemType;
            IsActive = false;
        }

        private void Update()
        {
            if (isActive) Move();
        }

        public void Interact(ItemType type)
        {
            if (type == Type) return;
            else if (type == ItemType.Goal) return;

            IsActive = false;
            gameObject.SetActive(false);
        }

        public void SpawnBullet()
        {
            gameObject.SetActive(true);
        }

        private void Move()
        {
            gameObject.transform.Translate(Vector2.up * movespeed * Time.deltaTime);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            try
            {
                IDamageable damageable = collision.GetComponent<IDamageable>();
                Interact(damageable.Type);
            }
            catch
            {
                if(collision.GetComponent<IInteractable>() == null || collision.GetComponent<IInteractable>().Type == ItemType.None)
                {
                    IsActive = false;
                    gameObject.SetActive(false);
                }
            }
        }
    }
}
