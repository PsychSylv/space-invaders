using SpaceInvader.Game.State;
using SpaceInvader.Item.Barrier;
using SpaceInvader.Item.Enemy.Pool;
using SpaceInvader.Item.Shield;
using SpaceInvader.Item.Type;
using SpaceInvader.Menu.Start;
using SpaceInvader.Player.Controller;
using SpaceInvader.UI.Popup.GameOver;
using SpaceInvader.UI.Popup.Pause;
using System;
using UnityEngine;

namespace SpaceInvader.Game.Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private StartMenuController start;
        [SerializeField] private PausePopupController pause;
        [SerializeField] private GameOverPopupController gameOver;
        [SerializeField] private PlayerController player;
        [SerializeField] private EnemyPoolController enemyPool;
        [SerializeField] private ShieldController[] shield;
        [SerializeField] private BarrierController barrier;
        
        private GameState gameState;
        private int numberOfEnemies;
        private int score = 0;
        private int life = 3;

        private const int MaxLife = 3;
        private const int Checkpoint = 10;

        public event Action<int> ScoreChanged;
        public event Action<int> LifeChanged;
        public event Action PlayerShoot;
        public event Action<ItemType> ItemDamaged;

        private void Start()
        {
            gameState = GameState.Home;

            start.StartPressed += StartGame;
            start.QuitPressed += OnQuit;

            pause.ContinuePressed += OnResume;
            pause.RestartPressed += OnRestart;
            pause.ExitPressed += OnExit;

            gameOver.PlayAgainPressed += OnRestart;
            gameOver.ExitPressed += OnExit;

            player.HitItem += OnItemHit;
            player.ShootBullet += OnPlayerShot;

            enemyPool.ItemHit += OnItemHit;
            enemyPool.EnemyDestroyed += OnEnemyDestroyed;

            barrier.EnemyWin += GameOverCheck;
        }

        private void OnDestroy()
        {
            start.StartPressed -= StartGame;
            start.QuitPressed -= OnQuit;

            pause.ContinuePressed -= OnResume;
            pause.RestartPressed -= OnRestart;
            pause.ExitPressed -= OnExit;

            gameOver.PlayAgainPressed -= OnRestart;
            gameOver.ExitPressed -= OnExit;

            player.HitItem -= OnItemHit;
            player.ShootBullet -= OnPlayerShot;

            enemyPool.ItemHit -= OnItemHit;
            enemyPool.EnemyDestroyed -= OnEnemyDestroyed;

            barrier.EnemyWin -= GameOverCheck;
        }

        private void Update()
        {
            if(gameState == GameState.Playing || gameState == GameState.Paused)
            {
                if(Input.GetKeyDown(KeyCode.Escape))
                {
                    Pause();
                }
            }
        }

        private void OnItemHit(ItemType item)
        {
            if(item == ItemType.Player)
            {
                life--;
                LifeChanged?.Invoke(life);
                ItemDamaged?.Invoke(item);

                if(life == 0)
                {
                    GameOverCheck();
                }
            }

            else if(item == ItemType.Enemy)
            {
                ItemDamaged?.Invoke(item);

                numberOfEnemies--;

                if(numberOfEnemies == 0)
                {
                    GameOverCheck();
                }

                if (numberOfEnemies % Checkpoint == 0 || numberOfEnemies == 1)
                {
                    enemyPool.UpdateSpeed();
                }
            }
        }

        private void OnPlayerShot()
        {
            PlayerShoot?.Invoke();
        }

        private void OnEnemyDestroyed(int point)
        {
            score += point;
            ScoreChanged?.Invoke(score);
        }

        private void StartGame()
        {
            start.HideCanvas();
            pause.HideCanvas();
            gameOver.HideCanvas();

            score = 0;
            life = MaxLife;
            numberOfEnemies = enemyPool.GetEnemyCount();

            for (int i = 0; i < shield.Length; i++)
            {
                shield[i].Reset();
            }

            gameState = GameState.Playing;

            ScoreChanged?.Invoke(score);
            LifeChanged?.Invoke(life);

            player.Reset();
            player.CanMove = true;

            enemyPool.ResetPool();
            enemyPool.InitializePosition();
            enemyPool.CanMove = true;
        }

        private void Pause()
        {
            if(gameState != GameState.Paused)
            {
                pause.ShowCanvas();

                player.CanMove = false;
                enemyPool.CanMove = false;

                gameState = GameState.Paused;
            }
            else
            {
                OnResume();
            }
        }

        private void GameOverCheck()
        {
            gameOver.ShowCanvas();

            enemyPool.ResetPool();
            enemyPool.CanMove = false;

            player.CanMove = false;
            gameState = GameState.GameOver;
        }

        private void OnResume()
        {
            player.CanMove = true;
            enemyPool.CanMove = true;

            pause.HideCanvas();

            gameState = GameState.Playing;
        }

        private void OnRestart()
        {
            StartGame();
        }

        private void OnExit()
        {
            gameOver.HideCanvas();
            pause.HideCanvas();
            start.ShowCanvas();

            gameState = GameState.Home;
        }

        private void OnQuit()
        {
            Application.Quit();
        }
    }
}
